package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper {
    public PacienteDto entityToDto(Paciente entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getId(),
                                ent.getDni(),
                                ent.getTelefono()
                        )
                )
                .orElse(new PacienteDto());
    }
    public Paciente dtoToEntity(PacienteDto dto) {
        Paciente entity = new Paciente();
        entity.setId(dto.getId());
        entity.setDni(dto.getDni());
        entity.setTelefono(dto.getTelefono());
        return entity;
    }
}

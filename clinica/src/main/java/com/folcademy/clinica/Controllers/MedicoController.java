package com.folcademy.clinica.Controllers;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicos")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }
    @GetMapping("")
    public ResponseEntity<List<MedicoDto>> ListarTodo() {
        return ResponseEntity.ok(medicoService.ListarTodos());
    }

    @GetMapping("/{idMedico}")
    public ResponseEntity<MedicoDto> listarUno(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.listarUno(id));
    }

    @PostMapping("")
    public ResponseEntity<MedicoDto> agregar(@RequestBody @Validated MedicoDto entity) {
        return ResponseEntity.ok(medicoService.agregar(entity));
    }

}

package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {

        this.pacienteService = pacienteService;
    }

    @GetMapping("")
    public ResponseEntity<List<PacienteDto>> listarTodos() {
        return ResponseEntity.ok(pacienteService.listarTodos());
    }
    @PostMapping("")
    public ResponseEntity<PacienteDto> agregar(@RequestBody @Validated PacienteDto entity) {
        return ResponseEntity.ok(pacienteService.agregar(entity));
    }


}
